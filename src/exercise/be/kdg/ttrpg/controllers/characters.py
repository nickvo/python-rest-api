# Created by nick at 3/19/19
from flask.json import jsonify

from solution.be.kdg.ttrpg.dom import Character
from solution.be.kdg.ttrpg.services import CharacterService, PlayerService, CampaignService

svc = CharacterService()
playerSvc = PlayerService()
campaignSvc = CampaignService()


def add(character: dict):
    player = playerSvc.get(character.get('player'))
    campaign_id = character.get('campaign')
    campaign = campaignSvc.get(campaign_id) if campaign_id is not None and character.get('campaign') > 0 else None

    dom: Character = Character(player, campaign, character.get('first_name'), character.get('last_name'),
                               character.get('gender'), character.get('race'), character.get('character_class'),
                               character.get('level'))
    return jsonify(to_dto(svc.make(dom)))


def collect(data_id: int):
    return jsonify(to_dto(svc.get(data_id)))


def change(data_id: int, character: dict):
    player = playerSvc.get(character.get('player'))
    campaign_id = character.get('campaign')
    campaign = campaignSvc.get(campaign_id) if campaign_id is not None and character.get('campaign') > 0 else None

    dom: Character = Character(player, campaign, character.get('first_name'), character.get('last_name'),
                               character.get('gender'), character.get('race'), character.get('character_class'),
                               character.get('level'))
    return jsonify(to_dto(svc.edit(data_id, dom)))


def destroy(data_id: int):
    svc.remove(data_id)


def collect_all():
    dtos = []
    list = svc.get_all()
    for o in (list if list is not None else []):
        dtos.append(to_dto(o))
    return jsonify(dtos)


def to_dto(dom: Character):
    return {
        'id': dom.id,
        'first_name': dom.first_name,
        'last_name': dom.last_name,
        'gender': dom.gender,
        'race': dom.race,
        'character_class': dom.character_class,
        'level': dom.level,
        'player': {'id': dom.player.id, 'name': dom.player.first_name + ' ' + dom.player.last_name},
        'campaign': {'id': dom.campaign.id, 'title': dom.campaign.title} if (dom.campaign is not None) else 'None'
    }
