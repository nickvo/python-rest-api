# Created by nick at 3/19/19
from flask.json import jsonify

from solution.be.kdg.ttrpg.dom import Player
from solution.be.kdg.ttrpg.services import PlayerService

svc = PlayerService()


def add(player: dict):
    dom: Player = Player(player.get('first_name'), player.get('last_name'), player.get('gender'))
    return jsonify(to_dto(svc.make(dom)))


def collect(data_id: int):
    return jsonify(to_dto(svc.get(data_id)))


def change(data_id: int, player: dict):
    dom: Player = Player(player.get('first_name'), player.get('last_name'), player.get('gender'))
    return jsonify(to_dto(svc.edit(data_id, dom)))


def destroy(data_id: int):
    svc.remove(data_id)


def collect_all():
    dtos = []
    list = svc.get_all()
    for o in (list if list is not None else []):
        dtos.append(to_dto(o))
    return jsonify(dtos)


def to_dto(dom: Player):
    return {
        'id': dom.id,
        'first_name': dom.first_name,
        'last_name': dom.last_name,
        'gender': dom.gender,
        'characters': list(map(lambda it: {'id': it.id, 'name': it.first_name + " " + it.last_name}, dom.characters)),
        'dms_campaigns': list(map(lambda it: {'id': it.id, 'title': it.title}, dom.dms_campaigns))
    }
