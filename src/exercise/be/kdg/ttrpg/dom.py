# Created by nick at 3/19/19


class Player(object):
    i = 0

    def __init__(
            self,
            first_name: str,
            last_name: str,
            gender: str
    ):
        self.i += 1
        self._id = self.i
        self._first_name = first_name
        self._last_name = last_name
        self._gender = gender
        self._characters = []
        self._dms_campaigns = []

    @property
    def id(self):
        return self._id

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, value):
        self._first_name = value

    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, value):
        self._last_name = value

    @property
    def gender(self):
        return self._gender

    @gender.setter
    def gender(self, value):
        self._gender = value

    @property
    def characters(self):
        return self._characters

    @characters.setter
    def characters(self, value):
        self._characters = value

    @property
    def dms_campaigns(self):
        return self._dms_campaigns

    @dms_campaigns.setter
    def dms_campaigns(self, value):
        self._dms_campaigns = value


class Campaign(object):
    i = 0

    def __init__(
            self,
            title: str,
            dm: Player,
            party: []
    ):
        self.i += 1
        self._id = self.i
        self._title = title
        self._dm = dm
        self._party = party

    @property
    def id(self):
        return self._id

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, value):
        self._title = value

    @property
    def dm(self):
        return self._dm

    @dm.setter
    def dm(self, value):
        self._dm = value

    @property
    def party(self):
        return self._party

    @party.setter
    def party(self, value):
        self._party = value


class Character(object):
    i = 0

    def __init__(
            self,
            player: Player,
            campaign: Campaign,
            first_name: str,
            last_name: str,
            gender: str,
            race: str,
            character_class: str,
            level=1
    ):
        self.i += 1
        self._id = self.i
        self._first_name = first_name
        self._last_name = last_name
        self._gender = gender
        self._race = race
        self._character_class = character_class
        self._level = level
        self._player = player
        self._campaign = campaign

    @property
    def id(self):
        return self._id

    @property
    def player(self):
        return self._player

    @player.setter
    def player(self, value):
        self._player = value

    @property
    def campaign(self):
        return self._campaign

    @campaign.setter
    def campaign(self, value):
        self._campaign = value

    @property
    def first_name(self):
        return self._first_name

    @first_name.setter
    def first_name(self, value):
        self._first_name = value

    @property
    def last_name(self):
        return self._last_name

    @last_name.setter
    def last_name(self, value):
        self._last_name = value

    @property
    def gender(self):
        return self._gender

    @gender.setter
    def gender(self, value):
        self._gender = value

    @property
    def race(self):
        return self._race

    @race.setter
    def race(self, value):
        self._race = value

    @property
    def character_class(self):
        return self._character_class

    @character_class.setter
    def character_class(self, value):
        self._character_class = value

    @property
    def level(self):
        return self._level

    @level.setter
    def level(self, value):
        self._level = value
