# Created by nick at 3/19/19
from flask.json import jsonify

from solution.be.kdg.ttrpg.dom import Campaign
from solution.be.kdg.ttrpg.services import CampaignService, PlayerService, CharacterService

svc = CampaignService()
playerSvc = PlayerService()
characterSvc = CharacterService()


def add(campaign: dict):
    dm_id = campaign.get('dm')
    dm = playerSvc.get(dm_id) if dm_id is not None and dm_id > 0 else None
    character_ids = campaign.get('party')
    characters = list(
        map(lambda c_id: characterSvc.get(c_id) if c_id is not None and c_id > 0 else None, character_ids))

    dom: Campaign = Campaign(campaign.get('title'), dm, characters)
    return jsonify(to_dto(svc.make(dom)))


def collect(data_id: int):
    return jsonify(to_dto(svc.get(data_id)))


def change(data_id: int, campaign: dict):
    dm_id = campaign.get('dm')
    dm = playerSvc.get(dm_id) if dm_id is not None and dm_id > 0 else None
    character_ids = campaign.get('party')
    characters = map(lambda c_id: characterSvc.get(c_id) if c_id is not None and c_id > 0 else None, character_ids)

    dom: Campaign = Campaign(campaign.get('title'), dm, characters)
    return jsonify(to_dto(svc.edit(data_id, dom)))


def destroy(data_id: int):
    svc.remove(data_id)


def collect_all():
    dtos = []
    list = svc.get_all()
    for o in (list if list is not None else []):
        dtos.append(to_dto(o))
    return jsonify(dtos)


def to_dto(dom: Campaign):
    return {
        'id': dom.id,
        'title': dom.title,
        'dm': {'id': dom.dm.id, 'name': dom.dm.first_name + ' ' + dom.dm.last_name} if dom.dm is not None else 'None',
        'party':
            list(map(lambda it: {'id': it.id,
                                 'name': it.first_name + ' ' + it.last_name} if dom.dm is not None else 'None',
                     dom.party))
    }
