# Created by nick at 3/19/19
from solution.be.kdg.ttrpg.dom import *


class IRepository:
    def create(self, data):
        raise NotImplementedError()

    def read(self, data_id):
        raise NotImplementedError()

    def update(self, data_id, data):
        raise NotImplementedError()

    def delete(self, data_id):
        raise NotImplementedError()

    def read_all(self):
        raise NotImplementedError()


class PlayerRepository(IRepository):
    _data = []

    def create(self, data: Player):
        self._data.append(data)
        return self._data[-1]

    def read(self, data_id: int):
        for i, o in enumerate(self._data):
            if o.id == data_id:
                return i, o
        return None

    def update(self, data_id: int, data: Player):
        index, new = self.read(data_id)

        if data.first_name is not None:
            new.first_name = data.first_name
        if data.last_name is not None:
            new.last_name = data.last_name
        if data.gender is not None:
            new.gender = data.gender
        if data.characters is not None:
            new.characters = data.characters
        if data.dms_campaigns is not None:
            new.dms_campaigns = data.dms_campaigns

        self._data[index] = new

        index, new = self.read(data_id)
        return new

    def delete(self, data_id: int):
        i, o = self.read(data_id)
        self._data.remove(o)

    def read_all(self):
        return self._data


class CharacterRepository(IRepository):
    _data = []

    def create(self, data: Character):
        self._data.append(data)
        return self.read_all()[-1]

    def read(self, data_id: int):
        for i, o in enumerate(self._data):
            if o.id == data_id:
                return i, o
        return None

    def update(self, data_id: int, data: Character):
        index, new = self.read(data_id)

        if data.first_name is not None:
            new.first_name = data.first_name
        if data.last_name is not None:
            new.last_name = data.last_name
        if data.gender is not None:
            new.gender = data.gender
        if data.race is not None:
            new.race = data.race
        if data.character_class is not None:
            new.character_class = data.character_class
        if data.level is not None:
            new.level = data.level
        if data.player is not None:
            new.player = data.player
        if data.campaign is not None:
            new.campaign = data.campaign

        self._data[index] = new

        index, new = self.read(data_id)
        return new

    def delete(self, data_id: int):
        i, o = self.read(data_id)
        self._data.remove(o)

    def read_all(self):
        return self._data


class CampaignRepository(IRepository):
    _data = []

    def create(self, data: Campaign):
        self._data.append(data)
        return self.read_all()[-1]

    def read(self, data_id: int):
        for i, o in enumerate(self._data):
            if o.id == data_id:
                return i, o
        return None

    def update(self, data_id: int, data: Campaign):
        index, new = self.read(data_id)

        if data.title is not None:
            new.title = data.title
        if data.dm is not None:
            new.dm = data.dm
        if data.party is not None:
            new.party = data.party

        self._data[index] = new

        index, new = self.read(data_id)
        return new

    def delete(self, data_id: int):
        i, o = self.read(data_id)
        self._data.remove(o)

    def read_all(self):
        return self._data
