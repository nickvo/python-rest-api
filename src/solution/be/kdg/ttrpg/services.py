# Created by nick at 3/19/19
from solution.be.kdg.ttrpg.repositories import *


class IService:
    def make(self, data):
        raise NotImplementedError()

    def get(self, data_id):
        raise NotImplementedError()

    def edit(self, data_id, data):
        raise NotImplementedError()

    def remove(self, data_id):
        raise NotImplementedError()

    def get_all(self):
        raise NotImplementedError()


class PlayerService(IService):
    class __Instance:
        def __init__(self):
            self.repo = PlayerRepository()
            self.character_svc = None
            self.campaign_svc = None
    _instance = None

    def __init__(self):
        if not PlayerService._instance:
            PlayerService._instance = PlayerService.__Instance()
            PlayerService._instance.character_svc = CharacterService()
            PlayerService._instance.campaign_svc = CampaignService()

    def make(self, data: Player):
        Player.i += 1
        new = self._instance.repo.create(data)
        self._update_connections(new)
        return new

    def get(self, data_id):
        i, o = self._instance.repo.read(data_id)
        return o

    def edit(self, data_id, data: Player):
        new = self._instance.repo.update(data_id, data)
        self._update_connections(new)
        return new

    def remove(self, data_id):
        return self._instance.repo.delete(data_id)

    def get_all(self):
        return self._instance.repo.read_all()

    def _update_connections(self, new: Player):
        # Update characters and campaigns
        for c in new.characters:
            if c.player and c.player.id != new.id:
                c.player = new
                self._instance.character_svc.edit(c.id, c)
        for c in new.dms_campaigns:
            if c.dm and c.dm.id != new.id:
                c.dm = new
                self._instance.campaign_svc.edit(c.id, c)


class CharacterService(IService):
    class __Instance:
        def __init__(self):
            self.repo = CharacterRepository()
            self.player_svc = None
            self.campaign_svc = None

    _instance = None

    def __init__(self):
        if not CharacterService._instance:
            CharacterService._instance = CharacterService.__Instance()
            CharacterService._instance.player_svc = PlayerService()
            CharacterService._instance.campaign_svc = CampaignService()

    def make(self, data: Character):
        Character.i += 1
        new = self._instance.repo.create(data)
        self._update_connections(new)
        return new

    def get(self, data_id):
        i, o = self._instance.repo.read(data_id)
        return o

    def edit(self, data_id, data: Character):
        new = self._instance.repo.update(data_id, data)
        self._update_connections(new)
        return new

    def remove(self, data_id):
        return self._instance.repo.delete(data_id)

    def get_all(self):
        return self._instance.repo.read_all()

    def _update_connections(self, new: Character):
        # Update player and campaign
        if new.player and not new.player.characters.__contains__(new):
            new.player.characters.append(new)
            self._instance.player_svc.edit(new.player.id, new.player)
        if new.campaign and not new.campaign.party.__contains__(new):
            new.campaign.party.append(new)
            self._instance.campaign_svc.edit(new.campaign.id, new.campaign)


class CampaignService(IService):
    class __Instance:
        def __init__(self):
            self.repo = CampaignRepository()
            self.player_svc = None
            self.character_svc = None

    _instance = None

    def __init__(self):
        if not CampaignService._instance:
            CampaignService._instance = CampaignService.__Instance()
            CampaignService._instance.player_svc = PlayerService()
            CampaignService._instance.character_svc = CharacterService()

    def make(self, data: Campaign):
        Campaign.i += 1

        new = self._instance.repo.create(data)
        self._update_connections(new)

        return new

    def get(self, data_id):
        i, o = self._instance.repo.read(data_id)
        return o

    def edit(self, data_id, data: Campaign):
        new = self._instance.repo.update(data_id, data)
        self._update_connections(new)
        return new

    def remove(self, data_id):
        return self._instance.repo.delete(data_id)

    def get_all(self):
        return self._instance.repo.read_all()

    def _update_connections(self, new: Campaign):
        # Update characters and player
        for c in new.party:
            if c.campaign and c.campaign.id != new.id:
                c.campaign = new
                self._instance.character_svc.edit(c.id, c)
        if new.dm and not new.dm.dms_campaigns.__contains__(new):
            new.dm.dms_campaigns.append(new)
            self._instance.player_svc.edit(new.dm.id, new.dm)
