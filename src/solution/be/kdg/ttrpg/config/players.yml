swagger: '2.0'
info:
  description: This is the swagger file that goes with our server code
  version: '1.0.0'
  title: TTRPG REST Article
consumes:
  - application/json
produces:
  - application/json

basePath: /api/players

definitions:
  Player:
    type: object
    properties:
      first_name:
        type: string
      last_name:
        type: string
      gender:
        type: string
      Players:
        type: array
        items:
          type: string
      dms_campaigns:
        type: array
        items:
          type: string
  AddPlayer:
    type: object
    properties:
      first_name:
        type: string
      last_name:
        type: string
      gender:
        type: string

# Paths supported by the server application
paths:
  /:
    get:
      operationId: controllers.players.collect_all
      tags:
        - Player
      summary: The player data structure supported by the server application
      description: Read the list of players
      responses:
        200:
          description: Successful read players list operation
          schema:
            type: array
            items:
              $ref: '#/definitions/Player'
    post:
      operationId: controllers.players.add
      tags:
        - Player
      summary: The player data structure supported by the server application
      description: Add a new player to the list
      parameters:
        - name: player
          in: body
          required: True
          description: The player data to be added
          schema:
            $ref: '#/definitions/AddPlayer'
      responses:
        201:
          description: Successful add player operation
          schema:
            $ref: '#/definitions/Player'
  '/{data_id}':
    get:
      operationId: controllers.players.collect
      tags:
        - Player
      summary: The player data structure supported by the server application
      description: Get a single player from the players list
      parameters:
        - name: data_id
          in: path
          required: True
          description: ID of player that needs to be fetched
          type: integer
      responses:
        200:
          description: Successful fetch player operation
          schema:
            $ref: '#/definitions/Player'
    put:
      operationId: controllers.players.change
      tags:
        - Player
      summary: The player data structure supported by the server application
      description: Update a player from the players list
      parameters:
        - name: data_id
          in: path
          required: True
          description: ID of player that needs to be updated
          type: integer
        - name: player
          in: body
          required: True
          description: player data to update
          schema:
            $ref: '#/definitions/Player'
      responses:
        200:
          description: Successful update player operation
          schema:
            $ref: '#/definitions/Player'
    delete:
      operationId: controllers.players.destroy
      tags:
        - Player
      summary: The player data structure supported by the server application
      description: Remove a player from the players list
      parameters:
        - name: data_id
          in: path
          required: True
          description: ID of player that needs to be removed
          type: integer
      responses:
        200:
          description: Successful remove player operation